//
//  ClockifyClient.h
//  ClockifyClient
//
//  Created by Jon Sturk on 2019-03-03.
//  Copyright © 2019 Jon Sturk. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for ClockifyClient.
FOUNDATION_EXPORT double ClockifyClientVersionNumber;

//! Project version string for ClockifyClient.
FOUNDATION_EXPORT const unsigned char ClockifyClientVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ClockifyClient/PublicHeader.h>


